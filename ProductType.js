var app = angular.module('responsive.coaches')

    .controller('ProductTypeController', ['$scope', '$element', '$timeout', 'Coach', function ($scope, $element, $timeout, Coach) {

        "use strict";
        angular.extend($scope, new Coach($scope, $element, $timeout));

        debugger;
        $scope.disableCreate = true;
        $scope.searchProductType = true;


        $scope.incServiceCallCounter = function () {
            $scope.$evalAsync(function () {
                $scope.serviceCallCount = 0;
                $scope.serviceCallCount++;
                console.log($scope.serviceCallCount);
                if ($scope.serviceCallCount > 0) {
                    $("#cover").show();
                }
            });

        }

        $scope.decServiceCallCounter = function () {
            $scope.$evalAsync(function () {
                $scope.serviceCallCount--;
                if ($scope.serviceCallCount <= 0) {
                    $('#cover').hide();
                }

            });
        }

        $scope.formatDateToDatePicker = function (date) {

            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
            // return [year, day, month].join('-');
        }

        var start;
        var alertTimeoutCallback = function (time) {

            start = Date.now();
            $timeout(function () {

                var end = Date.now();

                if ((end - start) / 1000 > 4) {

                    $('.alert').animate({
                        opacity: 0
                    }, 500, function () {

                        $timeout(function () {

                            $scope.alertModal = false;
                            $scope.$apply(function () {
                                $('.alert').css({
                                    'opacity': 1
                                });
                            });
                        }, 50);

                    });
                }

            }, time);
        }

        $scope.showSuccessMessage = function (customMessage) {
            $scope.alertModal = true;
            $scope.alertModalError = false;
            $scope.error = false;
            $scope.warning = false;
            $scope.success = true;
            $scope.errCode = "Success!!";
            $scope.errDesc = customMessage;

            $scope.$apply();

            alertTimeoutCallback(5000);
        }

        $scope.showErrorMessage = function (e, customMessage) {

            $scope.alertModal = true;
            $scope.alertModalError = true;
            $scope.error = true;
            $scope.success = false;

            $scope.warning = false;
            $scope.errCode = "Error!!";
            $scope.errDesc = customMessage;
            $scope.errDtls = JSON.parse(e.responseText).Data.errorMessage;

            $scope.$apply();

            alertTimeoutCallback(12000);
        }

        $scope.showWarningMessage = function (warningmsg) {
            $scope.alertModal = true;
            $scope.alertModalError = false;
            $scope.error = false;
            $scope.success = false;
            $scope.warning = true;
            $scope.customeError = false;
            $scope.errCode = "Warning!!";
            $scope.errDesc = warningmsg;


            alertTimeoutCallback(5000);

        }

        //reset
        $scope.reset = function () {
            $scope.disableCreate = true;
            $scope.ProductTypeForm.$setPristine();
            $scope.ProductTypeForm.$setUntouched();
            $scope.ProductTypeForm.$setSubmitted();
            $scope.AddProductType = {};
            $scope.ProductTypeSearchForm.$setPristine();
            $scope.ProductTypeSearchForm.$setUntouched();
            $scope.ProductTypeSearchForm.$setSubmitted();
            $scope.ProductTypeSearch = {};
            $scope.tableData = false;
            $scope.searchProductType = true;
            $scope.createProductType = false;

        }

        //Create Product Type
        $scope.AddProductType = {};
        $scope.saveProductType = function () {

            debugger;
            var input = {
                "PRODUCTTYPECODE": $scope.AddProductType.PRODUCTTYPECODE,
                "PRODUCTTYPEDESCRIPTION": $scope.AddProductType.PRODUCTTYPEDESCRIPTION
            };

            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {

                    $scope.disableCreate = true;
                    $scope.searchProductType = true;
                    $scope.createProductType = false;
                    $scope.AddProductType.PRODUCTTYPECODE = '';
                    $scope.AddProductType.PRODUCTTYPEDESCRIPTIONDESCRIPTION = '';
                    $scope.ProductTypeForm.$setPristine();
                    $scope.ProductTypeForm.$setUntouched();

                    $scope.showSuccessMessage("Product Type Inserted Successfully");
                    $scope.decServiceCallCounter();

                },
                error: function (e) {
                    $scope.showErrorMessage(e, "Error while Inserting Product Type");
                    $scope.decServiceCallCounter();
                }
            };

            $scope.incServiceCallCounter();
            $scope.context.options.addProductTypeService(serviceArgs);
        }

        //Product Type Search 

        $scope.ProductTypeSearch = {}
        $scope.getProductTypeData = function () {

            if ($scope.ProductTypeSearch.PRODUCTTYPECODE != "" && $scope.ProductTypeSearch.PRODUCTTYPECODE != undefined) {
                $scope.getProductTypeSearchData(1);
            } else {
                $scope.showWarningMessage("Search Product Type is missing!!!");
            }

        }

        $(document).ready(function () {

            $scope.getProductTypeSearchData = function (pageStart) {

                debugger;
                $scope.currentPosition = pageStart;

                var input = {
                    "TABLENAME": 'PRODUCTTYPE',
                    "LISTOFATTR": ['PRODUCTTYPECODE', 'PRODUCTTYPEDESCRIPTION'],
                    "LISTOFVALUES": [$scope.ProductTypeSearch.PRODUCTTYPECODE, $scope.ProductTypeSearch.PRODUCTTYPEDESCRIPTION],
                    "STARTPOSITION": $scope.currentPosition - 1,
                    "ORDERBY": "PRODUCTTYPECODE",
                    "NOOFROWS": 5
                };
                var serviceArgs = {

                    params: JSON.stringify(input),
                    load: function (data) {
                        debugger;

                        if (data.RESULT.items.length > 0) {
                            $scope.tableData = false;


                            $scope.ProductTypeList = data.RESULT.items;
                            $scope.resultCount = data.RESULTCOUNT.RESULTCOUNT;
                            $scope.assignPagVariables();
                            $scope.createProductType = false;


                        }
                        else {
                            $scope.ProductTypeList = {};
                            $scope.tableData = true;
                            $scope.totalPages = 0;
                            $scope.disableCreate = false;
                        }

                        $scope.decServiceCallCounter();
                        $scope.$apply();

                    },
                    error: function (e) {

                        $scope.decServiceCallCounter();
                        $scope.showErrorMessage(e, "Error while Getting Product Type Data");
                    }
                };

                $scope.incServiceCallCounter();
                $scope.context.options.getProductTypeDetails(serviceArgs);
            }

            //pager cordination

            $scope.currentPosition = 1;
            $scope.resultCount = 0;
            $scope.totalPages = 0;
            var pageSize = 5;

            $scope.assignPagVariables = function () {

                $scope.totalPages = Math.ceil($scope.resultCount / pageSize);

                switch ($scope.totalPages) {

                    case 1:
                        $scope.previousPage = 0;
                        $scope.nextPage = 0;
                        $scope.middlePage = 1;
                        break;
                    case 2:
                        if ($scope.currentPosition == 1) {
                            $scope.nextPage = 2;
                            $scope.previousPage = 0;

                        } else if ($scope.currentPosition == 2) {
                            $scope.previousPage = 1;
                            $scope.nextPage = 0;
                        }
                        $scope.middlePage = $scope.currentPosition;
                        break;
                    default:
                        if ($scope.currentPosition == 1) {

                            $scope.previousPage = 1;
                            $scope.middlePage = $scope.currentPosition + 1;
                            $scope.nextPage = $scope.currentPosition + 2;

                        } else if ($scope.currentPosition == $scope.totalPages) {

                            $scope.previousPage = $scope.currentPosition - 2;
                            $scope.middlePage = $scope.currentPosition - 1;
                            $scope.nextPage = $scope.currentPosition;

                        } else {
                            $scope.previousPage = $scope.currentPosition - 1;
                            $scope.middlePage = $scope.currentPosition;
                            $scope.nextPage = $scope.currentPosition + 1;
                        }
                }
            }

        });

    }]);
