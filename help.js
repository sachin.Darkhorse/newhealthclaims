var app = angular.module('responsive.coaches')
    .controller('ReportingClassController', ['$scope', '$element', '$timeout', 'Coach', function ($scope, $element, $timeout, Coach) {

        "use strict";
        angular.extend($scope, new Coach($scope, $element, $timeout));
        console.log("Catastrophe Master script logged . . .");

        $scope.disableCreate = true;
        $scope.searchClassPage = true;
        $scope.pageName = "Search";


        $scope.incServiceCallCounter = function () {
            $scope.$evalAsync(function () {
                $scope.serviceCallCount = 0;
                $scope.serviceCallCount++;
                console.log($scope.serviceCallCount);
                if ($scope.serviceCallCount > 0) {
                    $("#cover").show();
                }
            });

        }

        $scope.decServiceCallCounter = function () {

            $scope.$evalAsync(function () {
                $scope.serviceCallCount--;
                console.log($scope.serviceCallCount);
                if ($scope.serviceCallCount <= 0) {
                    $("#cover").hide();
                }
            });

        }


        var start;
        var alertTimeoutCallback = function (time) {

            start = Date.now();
            $timeout(function () {

                var end = Date.now();

                if ((end - start) / 1000 > 4) {

                    $('.alert').animate({
                        opacity: 0
                    }, 500, function () {

                        $timeout(function () {

                            $scope.alertModal = false;
                            $scope.IFSCalertModal = false;
                            $scope.$apply(function () {
                                $('.alert').css({
                                    'opacity': 1
                                });
                            });
                        }, 50);

                    });
                }

            }, time);
        }

        //Success Notification
        $scope.showSuccessMessage = function (customMessage) {
            $scope.alertModal = true;
            $scope.alertModalError = false;
            $scope.error = false;
            $scope.success = true;
            $scope.warning = false;
            $scope.errCode = "Success!!";
            $scope.errDesc = customMessage;

            $scope.$apply();

            alertTimeoutCallback(5000);
        }


        //Error Notification
        $scope.showErrorMessage = function (e, customMessage) {

            $scope.alertModal = true;
            $scope.alertModalError = true;
            $scope.error = true;
            $scope.success = false;
            $scope.warning = false;
            $scope.errCode = "Error!!";
            $scope.errDesc = customMessage;
            $scope.errDtls = JSON.parse(e.responseText).Data.errorMessage;

            $scope.$apply();

            alertTimeoutCallback(12000);
        }




        $scope.clearSavedClass = function () {

            $scope.CLASSCODE = '';
            $scope.CLASSDESCRIPTION = '';
            $scope.classSearchForm.$setPristine();
            $scope.classSearchForm.$setUntouched();
            $scope.classSearchForm.$setSubmitted();
            $scope.reportingClassForm.$setPristine();
            $scope.reportingClassForm.$setUntouched();
            $scope.reportingClassForm.$setSubmitted();

            $scope.pageName = "Search";
            $scope.disableCreate = true;
            $scope.searchClassPage = true;
            $scope.newReportingClassPage = false;

        }

        //this method is saving class code and class description for new LOB
        $scope.saveData = function () {
            if ($scope.CLASSCODE != "" && $scope.CLASSCODE != undefined && $scope.CLASSDESCRIPTION != "" && $scope.CLASSDESCRIPTION != undefined) {
                $scope.saveNewReportingClass();
            } else { }
        }


        $scope.saveNewReportingClass = function () {

            debugger;
            var input = {
                "CLASSCODE": $scope.CLASSCODE,
                "CLASSDESCRIPTION": $scope.CLASSDESCRIPTION

            };
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {

                    $scope.clearSavedClass();
                    $scope.showSuccessMessage("Record Inserted Successfully");
                    $scope.decServiceCallCounter();

                },
                error: function (e) {
                    $scope.showErrorMessage(e, "Error while Inserting Record");
                    $scope.decServiceCallCounter();
                }
            };

            $scope.incServiceCallCounter();
            $scope.context.options.addNewReportingClass(serviceArgs);
        }




        //this is fetching data from database

        $scope.checkClassCode = function () {
            if ($scope.CLASSCODE != "" && $scope.CLASSCODE != undefined) {
                $scope.getReportingClass();
            } else { }
        }


        $scope.getReportingClass = function () {

            if ($scope.CLASSCODE != "" && $scope.CLASSCODE != undefined) {
                $scope.getReportingClassData(1);
            } else { }
        }

        $(document).ready(function () {
            $scope.getReportingClassData = function (pageStart) {

                debugger;
                $scope.currentPosition = pageStart;

                $scope.listOfAttr = [];
                $scope.listOfValues = [];
                if ($scope.CLASSDESCRIPTION != "" && $scope.CLASSDESCRIPTION != undefined) {
                    $scope.listOfAttr = ["CLASSCODE", "CLASDESCRIPTION"];
                    $scope.listOfValues = [$scope.CLASSCODE, $scope.CLASSDESCRIPTION];
                } else {
                    $scope.listOfAttr = ["CLASSCODE"];
                    $scope.listOfValues = [$scope.CLASSCODE];
                }

                var input = {
                    "TABLENAME": "LINEOFBUSINESSCLASSTABLE",
                    "LISTOFATTR": $scope.listOfAttr,
                    "LISTOFVALUES": $scope.listOfValues,
                    "STARTPOSITION": $scope.currentPosition - 1,
                    "ORDERBY": "CLASSCODE",
                    "NOOFROWS": 5
                }

                var serviceArgs = {
                    params: JSON.stringify(input),
                    load: function (data) {

                        if (data.RESULT.items.length > 0) {
                            $scope.tableData = false;

                            $scope.SEARCHEDCLASS = data.RESULT.items;
                            $scope.resultCount = data.RESULTCOUNT.RESULTCOUNT;
                            $scope.assignPagVariables();

                            $scope.newReportingClassPage = false;
                            $scope.disableCreate = true;
                        }
                        else {
                            $scope.SEARCHEDCLASS = [];
                            $scope.tableData = true;
                            $scope.totalPages = 0;

                            $scope.disableCreate = false;
                        }

                        $scope.decServiceCallCounter();
                        $scope.$apply();
                    },

                    error: function (e) {
                        $scope.showErrorMessage(e, "Error while Fetching Data");
                        $scope.decServiceCallCounter();
                    }
                };

                $scope.incServiceCallCounter();
                $scope.context.options.getReportingClassData(serviceArgs);

            }

            $scope.currentPosition = 1;
            $scope.resultCount = 0;
            $scope.totalPages = 0;
            var pageSize = 5;

            $scope.assignPagVariables = function () {

                $scope.totalPages = Math.ceil($scope.resultCount / pageSize);

                switch ($scope.totalPages) {

                    case 1:
                        $scope.previousPage = 0;
                        $scope.nextPage = 0;
                        $scope.middlePage = 1;
                        break;
                    case 2:
                        if ($scope.currentPosition == 1) {
                            $scope.nextPage = 2;
                            $scope.previousPage = 0;

                        } else if ($scope.currentPosition == 2) {
                            $scope.previousPage = 1;
                            $scope.nextPage = 0;
                        }
                        $scope.middlePage = $scope.currentPosition;
                        break;
                    default:
                        if ($scope.currentPosition == 1) {

                            $scope.previousPage = 1;
                            $scope.middlePage = $scope.currentPosition + 1;
                            $scope.nextPage = $scope.currentPosition + 2;

                        } else if ($scope.currentPosition == $scope.totalPages) {

                            $scope.previousPage = $scope.currentPosition - 2;
                            $scope.middlePage = $scope.currentPosition - 1;
                            $scope.nextPage = $scope.currentPosition;

                        } else {
                            $scope.previousPage = $scope.currentPosition - 1;
                            $scope.middlePage = $scope.currentPosition;
                            $scope.nextPage = $scope.currentPosition + 1;
                        }

                }

            }
        });

    }]);
